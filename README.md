# Flightscope checklists

### During build the following strings are substituted:

* `${CI_PAGES_DOMAIN}`
* `${CI_PAGES_DOMAIN}`
* `${CI_PAGES_URL}`
* `${CI_PAGES_URL}`
* `${CI_PROJECT_TITLE}`
* `${CI_PROJECT_URL}`
* `${COMMIT_TIME}`
* `${GITLAB_USER_NAME}`
* `${GITLAB_USER_EMAIL}`
* `${CI_COMMIT_SHA}`
* `${CI_PROJECT_VISIBILITY}`

### Aircraft Booklet recommended card size

* Side with no pins: `220mm x 172mm`
* Side with pins: `231mm x 169mm`
* Print at A4 using `evince` with 81% scaling to fit aircraft book
* Laminate with A4
* Guillotine to aircraft book size
